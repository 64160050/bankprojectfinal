/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.tanisorn.bankproject;

/**
 *
 * @author uSeR
 */
public class BankProject {

    private int balance;
     public BankProject( int balance) {
        this.balance = balance;
     }

    @Override
    public String toString() {
        return " " + balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }
    public int Withdraw(int amount) {
         return balance = balance - amount;
    }
    public int Deposit(int amount) {
        return balance = balance + amount; 
    }
}
